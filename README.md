# gauthnqr

Google Auth / FreeOTP / mod-authn-otp / pam_oath Account Generator

		2016 Thomas Zink <tz at uni dot kn>
		ga.php Copyright (C) 2012 CNY Support, LLC
		phpqrencode Copyright (C) 2010 Dominik Dzienia <deltalab at poczta dot fm>
		This library is free software; you can redistribute it and/or
		modify it under the terms of the GNU Lesser General Public
		License as published by the Free Software Foundation; either
		version 3 of the License, or any later version.
		
		This library is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
		Lesser General Public License for more details.
		
		You should have received a copy of the GNU Lesser General Public
		License along with this library; if not, write to the Free Software
		Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA


This software is a utility for managing accounts for Google Authenticator / FreeOTP and Apache mod-authn-otp / Linux pam-oath.
When used, it creates:

- If needed a random secret key
- A QR code to scan with Google Authenticator / FreeOTP
- A configuration to copy to the otp.users / users_oath file
- Commands to program a yubikey for wither slot 1 or 2

It can be used to config Apache mod-authn-otp, Linux ssh / offline login with pam-oath, yubikey usb tokens, software tokens like Google Authenticator or FreeOTP.
It supports both HOTP or TOTP and it only uses local libraries to generate secret key and the QR code.
The secret is never send to any third party.
Still, you should always use SSL/TLS when submitting any personal information.

- Based on [cnysupport google auth generator](https://www.cnysupport.com/ga.txt.)
- Also based on [googleauthqrcode](https://github.com/carkrueger/googleauthqrcode)
- Uses [phpqrencode](http://sourceforge.net/projects/phpqrcode/) to generate QR code
- Uses a modified version of [PHP base_conversion](https://pgregg.com/projects/php/base_conversion/base_conversion.inc.phps) for secret conversion
- Uses a modified version of [PHPGangsta GoogleAuthenticator](https://github.com/PHPGangsta/GoogleAuthenticator) to generate secret key

## Requirements

To run this software you need

* Webserver
* PHP
* php-gd

For example on a Debian system install the packages:

		sudo apt-get install apache2 php5 php5-gd libapache2-mod-php5

To actually use Apache mod-authn-otp look here [mod-authn-otp](https://github.com/archiecobbs/mod-authn-otp)

## How to use

Copy to your webserver, then navigate to index.html and follow instructions.

You can also directly navigate to ga.php using the following syntax:

		<server>/<path>/ga.php?site=<sitename>&user=<username>[&secret=<optional secret in hex>]&algo=[HOTP,TOTP]

Parameters are:
- site: required. Name of the site you wish to authenticate with
- user: required. User name
- secret: optional. The secret key in hex encoding that is used to generate QR code and the otp.user file entry
- algo: required. The algorithm to use, either HOTP or TOTP

## References

- [using-google-authenticator-with-apache-mod_authn_otp](https://www.cnysupport.com/index.php/free-stuff/using-google-authenticator-with-apache-mod_authn_otp)
- [one-time-passwords-hotp-and-totp](http://blogs.forgerock.org/petermajor/2014/02/one-time-passwords-hotp-and-totp/)
- [Google Authenticator](https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=en)
- [FreeOTP](https://play.google.com/store/apps/details?id=org.fedorahosted.freeotp&hl=en)